package mx.com.gm.web;

import java.util.List;
import mx.com.gm.domain.Pokemon;
import mx.com.gm.servicio.PokemonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/rest")
public class PokemonRestController {
    
    @Autowired
    private PokemonService pokemonService;

    PokemonRestController(PokemonService pokemonServiceMock) {
        pokemonService = pokemonServiceMock;
    }
    
    @GetMapping(value = "/listar")
    public List<Pokemon> listarPokemons(){
        return pokemonService.listarPokemons();
    }
    
    @GetMapping(value = "/encontrar/{id}")
    public ResponseEntity<Pokemon> encontrar(@PathVariable Long id){
        Pokemon pok = pokemonService.encontrarPokemonPorId(id);
        if (pok == null){
            return new ResponseEntity(pok, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(pok, HttpStatus.OK);
    }
    
    @PostMapping(value = "/guardar")
    public ResponseEntity<Pokemon> guardar(@RequestBody Pokemon pokemon){
        Pokemon pok = pokemonService.guardar(pokemon);
        return new ResponseEntity(pok, HttpStatus.OK);
    }
    
    @GetMapping(value = "/eliminar/{id}")
    public ResponseEntity<Pokemon> eliminar(@PathVariable Long id){
        Pokemon pok = pokemonService.encontrarPokemonPorId(id);
        if (pok == null){
            return new ResponseEntity(pok, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        pokemonService.eliminarPorId(id);
        return new ResponseEntity(pok, HttpStatus.OK);
    }
    
    @GetMapping(value = "/evolucion/{id}")
    public ResponseEntity<Pokemon> mostrarEvolucion(@PathVariable Long id){
        Pokemon pokemon = pokemonService.encontrarPokemonPorId(id);
        if (pokemon == null){
            return new ResponseEntity(pokemon, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        Pokemon evolucion = pokemonService.encontrarEvolucion(pokemon);
        return new ResponseEntity(evolucion, HttpStatus.OK);
    }
}
