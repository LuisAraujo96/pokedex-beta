package mx.com.gm.web;

import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import mx.com.gm.domain.Pokemon;
import mx.com.gm.servicio.PokemonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
@Slf4j
public class ControladorInicio {
    
    @Autowired
    private PokemonService pokemonService;
    
    @GetMapping("/login")
    public String login(Model model) {
        return "login";
    }

    @GetMapping("/")
    public String inicio(Model model) {
        var pokemons = pokemonService.listarPokemons();

        log.info("ejecutando el controlador Spring MVC");
        model.addAttribute("pokemons", pokemons);
        return "index";
    }
    
    @GetMapping("/agregar")
    public String agregar(Pokemon pokemon) {
        return "modificar";
    }
    
    @PostMapping("/guardar")
    public String guardar(@Valid Pokemon pokemon, Errors errores){
        if(errores.hasErrors()){
            return "modificar";
        }
        pokemonService.guardar(pokemon);
        return "redirect:/";
    }
    
    @GetMapping("/editar/{id_pokemon}")
    public String editar(Pokemon pokemon, Model model){
        pokemon = pokemonService.encontrarPokemon(pokemon);
        model.addAttribute("pokemon", pokemon);
        return "modificar";
    }
    
    @GetMapping("/eliminar")
    public String eliminar(Pokemon pokemon){
        pokemonService.eliminar(pokemon);
        return "redirect:/";
    }
    
    @GetMapping("/evolucionar/{id_pokemon}/{id_evolucion}")
    public String evolucionar(Pokemon pokemon, Model model){
        Pokemon evolucion = pokemonService.encontrarEvolucion(pokemon);
        model.addAttribute("evolucion", evolucion);
        return "evolucionar";
    }
}
