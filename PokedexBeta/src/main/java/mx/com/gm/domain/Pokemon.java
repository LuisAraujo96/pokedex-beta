package mx.com.gm.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import lombok.Data;

@Data
@Entity
@Table(name = "pokemon")
public class Pokemon implements Serializable{
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_pokemon;
    
    @NotEmpty
    private String pokemon_nombre;
    
    @NotEmpty
    private Long pokemon_nivel;
    
    @NotEmpty
    private String pokemon_tipos;
    
    @NotEmpty
    private String pokemon_habilidades;
    
    @NotEmpty
    private Long id_evolucion;
}
