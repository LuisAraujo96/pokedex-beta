package mx.com.gm.dao;

import mx.com.gm.domain.Pokemon;
import org.springframework.data.repository.CrudRepository;


public interface PokemonDao extends CrudRepository<Pokemon, Long>{
    
}
