package mx.com.gm.servicio;

import java.util.List;
import mx.com.gm.dao.PokemonDao;
import mx.com.gm.domain.Pokemon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PokemonServiceImpl implements PokemonService {
    
    @Autowired
    private PokemonDao pokemonDao;

    PokemonServiceImpl(PokemonDao pokemonDaoMock) {
        pokemonDao = pokemonDaoMock;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Pokemon> listarPokemons() {
        return (List<Pokemon>) pokemonDao.findAll();
    }

    @Override
    @Transactional
    public Pokemon guardar(Pokemon pokemon) {
        return pokemonDao.save(pokemon);
    }

    @Override
    @Transactional
    public void eliminar(Pokemon pokemon) {
        pokemonDao.delete(pokemon);
    }
    
    @Override
    @Transactional
    public void eliminarPorId(Long id){
        pokemonDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Pokemon encontrarPokemon(Pokemon pokemon){
        return pokemonDao.findById(pokemon.getId_pokemon()).orElse(null);
    }
    
    @Override
    @Transactional(readOnly = true)
    public Pokemon encontrarPokemonPorId(Long id){
        return pokemonDao.findById(id).orElse(null);
    }
    
    @Override
    @Transactional(readOnly = true)
    public Pokemon encontrarEvolucion(Pokemon pokemon){
        
        Pokemon evolucion = null;
        
        if (pokemon.getId_evolucion() != null){
            evolucion = pokemonDao.findById(pokemon.getId_evolucion()).orElse(null);
        }
        return evolucion;
    }
}
