package mx.com.gm.servicio;

import java.util.List;
import mx.com.gm.domain.Pokemon;

public interface PokemonService {
    
    public List<Pokemon> listarPokemons();
    
    public Pokemon guardar(Pokemon pokemon);
    
    public void eliminar(Pokemon pokemon);
    
    public void eliminarPorId(Long id);
    
    public Pokemon encontrarPokemon(Pokemon pokemon);
    
    public Pokemon encontrarPokemonPorId(Long id);
    
    public Pokemon encontrarEvolucion(Pokemon pokemon);
}
