package mx.com.gm.web;

import java.util.Arrays;
import java.util.List;
import mx.com.gm.domain.Pokemon;
import mx.com.gm.servicio.PokemonService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import static org.mockito.Mockito.times;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class PokemonRestControllerTest {
    
    PokemonService pokemonServiceMock = Mockito.mock(PokemonService.class);
        
    @Autowired
    PokemonRestController pokemonRestController = new PokemonRestController(pokemonServiceMock);
    
    @BeforeEach
    void setUp() {
        Pokemon snorlax = new Pokemon();
        snorlax.setId_pokemon(Long.valueOf(100));
        snorlax.setPokemon_nombre("Snorlax");
        snorlax.setPokemon_nivel(Long.valueOf(30));
        snorlax.setPokemon_tipos("Normal");
        snorlax.setPokemon_habilidades("Ronquido");
        snorlax.setId_evolucion(Long.valueOf(0));
        
        Pokemon eevee = new Pokemon();
        eevee.setId_pokemon(Long.valueOf(101));
        eevee.setPokemon_nombre("Eevee");
        eevee.setPokemon_nivel(Long.valueOf(30));
        eevee.setPokemon_tipos("Normal");
        eevee.setPokemon_habilidades("Excavar");
        eevee.setId_evolucion(Long.valueOf(102));
        
        List<Pokemon> listado = Arrays.asList(snorlax, eevee);
        
        Mockito.when(pokemonServiceMock.listarPokemons()).thenReturn(listado);
        
        Mockito.when(pokemonServiceMock.encontrarPokemonPorId(Long.valueOf(100))).thenReturn(snorlax);
        
        Mockito.when(pokemonServiceMock.encontrarPokemonPorId(Long.valueOf(99))).thenReturn(null);
        
        Mockito.when(pokemonServiceMock.guardar(eevee)).thenReturn(eevee);
        
        Mockito.when(pokemonServiceMock.encontrarPokemonPorId(Long.valueOf(101))).thenReturn(eevee);
        
        Pokemon flareon = new Pokemon();
        flareon.setId_pokemon(Long.valueOf(102));
        flareon.setPokemon_nombre("Flareon");
        flareon.setPokemon_nivel(Long.valueOf(40));
        flareon.setPokemon_tipos("Fuego");
        flareon.setPokemon_habilidades("Mordisco");
        flareon.setId_evolucion(Long.valueOf(0));
        
        Mockito.when(pokemonServiceMock.encontrarEvolucion(eevee)).thenReturn(flareon);

        Mockito.when(pokemonServiceMock.encontrarPokemonPorId(Long.valueOf(102))).thenReturn(flareon);
        Mockito.when(pokemonServiceMock.encontrarEvolucion(flareon)).thenReturn(null);
    }
    
    @Test
    void test01PidoElListadoDePokemonesYVerificoQueSeLlamoAlMetodoParaListarlos(){
        List<Pokemon> lista = pokemonRestController.listarPokemons();
        
        Assertions.assertEquals("Snorlax", lista.get(0).getPokemon_nombre());
        Assertions.assertEquals("Eevee", lista.get(1).getPokemon_nombre());
        
        Mockito.verify(pokemonServiceMock, times(1)).listarPokemons();
    }
    
    @Test
    void test02PidoEncontrarAUnPokemonGuardadoPorLaID100YVerificoQueSeLlamaAlMetodoCorrectoParaEncontrarlo(){
        Pokemon snorlax = new Pokemon();
        snorlax.setId_pokemon(Long.valueOf(100));
        snorlax.setPokemon_nombre("Snorlax");
        snorlax.setPokemon_nivel(Long.valueOf(30));
        snorlax.setPokemon_tipos("Normal");
        snorlax.setPokemon_habilidades("Ronquido");
        snorlax.setId_evolucion(Long.valueOf(0));
        ResponseEntity respuestaEsperada = new ResponseEntity(snorlax, HttpStatus.OK);
        
        ResponseEntity respuestaDevuelta = pokemonRestController.encontrar(Long.valueOf(100));
        
        Assertions.assertEquals(respuestaEsperada, respuestaDevuelta);
        
        Mockito.verify(pokemonServiceMock, times(1)).encontrarPokemonPorId(Long.valueOf(100));
    }
    
    @Test
    void test03PidoEncontrarAUnPokemonNoGuardadoPorLaID99YVerificoQueSeLlamaAlMetodoCorrectoParaIntentarEncontrarlo(){
        ResponseEntity respuestaEsperada = new ResponseEntity(null, HttpStatus.INTERNAL_SERVER_ERROR);
        
        ResponseEntity respuestaDevuelta = pokemonRestController.encontrar(Long.valueOf(99));
        
        Assertions.assertEquals(respuestaEsperada, respuestaDevuelta);
        
        Mockito.verify(pokemonServiceMock, times(1)).encontrarPokemonPorId(Long.valueOf(99));
    }
    
    @Test
    void test04GuardoElPokemonEeveeYVerificoQueSeLlameAlMetodoCorrespondiente(){
        Pokemon eevee = new Pokemon();
        eevee.setId_pokemon(Long.valueOf(101));
        eevee.setPokemon_nombre("Eevee");
        eevee.setPokemon_nivel(Long.valueOf(30));
        eevee.setPokemon_tipos("Normal");
        eevee.setPokemon_habilidades("Excavar");
        eevee.setId_evolucion(Long.valueOf(102));
        ResponseEntity respuestaEsperada = new ResponseEntity(eevee, HttpStatus.OK);
        
        ResponseEntity respuestaDevuelta = pokemonRestController.guardar(eevee);
        
        Assertions.assertEquals(respuestaEsperada, respuestaDevuelta);
        
        Mockito.verify(pokemonServiceMock, times(1)).guardar(eevee);
    }
    
    @Test
    void test05PidoBorrarAUnPokemonGuardadoConLaID101YVerificoQueSeLlamaAlMetodoCorrecto(){
        Pokemon eevee = new Pokemon();
        eevee.setId_pokemon(Long.valueOf(101));
        eevee.setPokemon_nombre("Eevee");
        eevee.setPokemon_nivel(Long.valueOf(30));
        eevee.setPokemon_tipos("Normal");
        eevee.setPokemon_habilidades("Excavar");
        eevee.setId_evolucion(Long.valueOf(102));
        ResponseEntity respuestaEsperada = new ResponseEntity(eevee, HttpStatus.OK);
        
        ResponseEntity respuestaDevuelta = pokemonRestController.eliminar(Long.valueOf(101));
        
        Assertions.assertEquals(respuestaEsperada, respuestaDevuelta);
        
        Mockito.verify(pokemonServiceMock, times(1)).encontrarPokemonPorId(Long.valueOf(101));
    }
    
    @Test
    void test06PidoBorrarAUnPokemonNoGuardadoConLaID99YVerificoQueSeLlamaAlMetodoCorrecto(){
        ResponseEntity respuestaEsperada = new ResponseEntity(null, HttpStatus.INTERNAL_SERVER_ERROR);
        
        ResponseEntity respuestaDevuelta = pokemonRestController.eliminar(Long.valueOf(99));
        
        Assertions.assertEquals(respuestaEsperada, respuestaDevuelta);
        
        Mockito.verify(pokemonServiceMock, times(1)).encontrarPokemonPorId(Long.valueOf(99));
    }
    
    @Test
    void test07PidoVerLaEvolucionDeEeveeYVerificoQueSeLlameAlMetodoCorrecto(){
        Pokemon eevee = new Pokemon();
        eevee.setId_pokemon(Long.valueOf(101));
        eevee.setPokemon_nombre("Eevee");
        eevee.setPokemon_nivel(Long.valueOf(30));
        eevee.setPokemon_tipos("Normal");
        eevee.setPokemon_habilidades("Excavar");
        eevee.setId_evolucion(Long.valueOf(102));
        
        Pokemon flareon = new Pokemon();
        flareon.setId_pokemon(Long.valueOf(102));
        flareon.setPokemon_nombre("Flareon");
        flareon.setPokemon_nivel(Long.valueOf(40));
        flareon.setPokemon_tipos("Fuego");
        flareon.setPokemon_habilidades("Mordisco");
        flareon.setId_evolucion(Long.valueOf(0));
        ResponseEntity respuestaEsperada = new ResponseEntity(flareon, HttpStatus.OK);
        
        ResponseEntity respuestaDevuelta = pokemonRestController.mostrarEvolucion(Long.valueOf(101));
        
        Assertions.assertEquals(respuestaEsperada, respuestaDevuelta);
        
        Mockito.verify(pokemonServiceMock, times(1)).encontrarPokemonPorId(Long.valueOf(101));
        Mockito.verify(pokemonServiceMock, times(1)).encontrarEvolucion(eevee);
    }
    
    @Test
    void test08PidoVerLaEvolucionDeFlareonYVerificoQueSeLlameAlMetodoCorrecto(){
        Pokemon flareon = new Pokemon();
        flareon.setId_pokemon(Long.valueOf(102));
        flareon.setPokemon_nombre("Flareon");
        flareon.setPokemon_nivel(Long.valueOf(40));
        flareon.setPokemon_tipos("Fuego");
        flareon.setPokemon_habilidades("Mordisco");
        flareon.setId_evolucion(Long.valueOf(0));
        ResponseEntity respuestaEsperada = new ResponseEntity(null, HttpStatus.OK);
        
        ResponseEntity respuestaDevuelta = pokemonRestController.mostrarEvolucion(Long.valueOf(102));
        
        Assertions.assertEquals(respuestaEsperada, respuestaDevuelta);
        
        Mockito.verify(pokemonServiceMock, times(1)).encontrarPokemonPorId(Long.valueOf(102));
        Mockito.verify(pokemonServiceMock, times(1)).encontrarEvolucion(flareon);
    }
    
    @Test
    void test09PidoVerLaEvolucionDeUnPokemonNoGuardadoYVerificoQueSeLlameAlMetodoCorrecto(){
        ResponseEntity respuestaEsperada = new ResponseEntity(null, HttpStatus.INTERNAL_SERVER_ERROR);
        
        ResponseEntity respuestaDevuelta = pokemonRestController.mostrarEvolucion(Long.valueOf(99));
        
         Assertions.assertEquals(respuestaEsperada, respuestaDevuelta);
        
        Mockito.verify(pokemonServiceMock, times(1)).encontrarPokemonPorId(Long.valueOf(99));
    }
}
