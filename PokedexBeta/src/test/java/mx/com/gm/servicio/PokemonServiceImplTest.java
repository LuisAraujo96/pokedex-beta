package mx.com.gm.servicio;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import mx.com.gm.dao.PokemonDao;
import mx.com.gm.domain.Pokemon;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import static org.mockito.Mockito.times;
import org.springframework.beans.factory.annotation.Autowired;

public class PokemonServiceImplTest {
    
    PokemonDao pokemonDaoMock = Mockito.mock(PokemonDao.class);
    
    @Autowired
    PokemonServiceImpl pokemonServiceImpl = new PokemonServiceImpl(pokemonDaoMock);
    
    @BeforeEach
    void setUp(){
        Pokemon snorlax = new Pokemon();
        snorlax.setId_pokemon(Long.valueOf(100));
        snorlax.setPokemon_nombre("Snorlax");
        snorlax.setPokemon_nivel(Long.valueOf(30));
        snorlax.setPokemon_tipos("Normal");
        snorlax.setPokemon_habilidades("Ronquido");
        snorlax.setId_evolucion(Long.valueOf(0));
        
        Optional<Pokemon> oSnorlax = Optional.of(snorlax);
        
        Mockito.when(pokemonDaoMock.findById(Long.valueOf(100))).thenReturn(oSnorlax);
        
        Pokemon eevee = new Pokemon();
        eevee.setId_pokemon(null);
        eevee.setPokemon_nombre("Eevee");
        eevee.setPokemon_nivel(Long.valueOf(30));
        eevee.setPokemon_tipos("Normal");
        eevee.setPokemon_habilidades("Excavar");
        eevee.setId_evolucion(Long.valueOf(102));
        
        Mockito.when(pokemonDaoMock.save(eevee)).thenReturn(eevee);
        
        List<Pokemon> listado = Arrays.asList(snorlax, eevee);
        
        Mockito.when(pokemonDaoMock.findAll()).thenReturn(listado);
        
        Pokemon flareon = new Pokemon();
        flareon.setId_pokemon(Long.valueOf(102));
        flareon.setPokemon_nombre("Flareon");
        flareon.setPokemon_nivel(Long.valueOf(40));
        flareon.setPokemon_tipos("Fuego");
        flareon.setPokemon_habilidades("Mordisco");
        flareon.setId_evolucion(Long.valueOf(0));
        
        Optional<Pokemon> oFlareon = Optional.of(flareon);
        
        Mockito.when(pokemonDaoMock.findById(Long.valueOf(102))).thenReturn(oFlareon);
    }
    
    @Test
    void test01PidoElListadoDePokemonesYVerificoQueSeLlamoAlMetodoParaListarlos(){
        List<Pokemon> lista = pokemonServiceImpl.listarPokemons();
        
        Assertions.assertEquals("Snorlax", lista.get(0).getPokemon_nombre());
        Assertions.assertEquals("Eevee", lista.get(1).getPokemon_nombre());
        
        Mockito.verify(pokemonDaoMock, times(1)).findAll();
    }
    
    @Test
    void test02GuardoAEeveeYVerificoQueSeHayaLlamadoAlMetodoParaGuardarlo(){
        Pokemon eevee = new Pokemon();
        eevee.setId_pokemon(null);
        eevee.setPokemon_nombre("Eevee");
        eevee.setPokemon_nivel(Long.valueOf(30));
        eevee.setPokemon_tipos("Normal");
        eevee.setPokemon_habilidades("Excavar");
        eevee.setId_evolucion(Long.valueOf(102));
        
        Pokemon pokemonGuardado = pokemonServiceImpl.guardar(eevee);
        
        Assertions.assertEquals(null, pokemonGuardado.getId_pokemon());
        Assertions.assertEquals("Eevee", pokemonGuardado.getPokemon_nombre());
        Assertions.assertEquals(Long.valueOf(30), pokemonGuardado.getPokemon_nivel());
        Assertions.assertEquals("Normal", pokemonGuardado.getPokemon_tipos());
        Assertions.assertEquals("Excavar", pokemonGuardado.getPokemon_habilidades());
        Assertions.assertEquals(Long.valueOf(102), pokemonGuardado.getId_evolucion());
        
        Mockito.verify(pokemonDaoMock, times(1)).save(eevee);
    }
    
    @Test
    void test03PidoBorrarAlPokemonEeveeYVerificoQueSehayaLlamadoAlMetodoParaBorrarlo(){
        Pokemon eevee = new Pokemon();
        eevee.setId_pokemon(Long.valueOf(101));
        eevee.setPokemon_nombre("Eevee");
        eevee.setPokemon_nivel(Long.valueOf(30));
        eevee.setPokemon_tipos("Normal");
        eevee.setPokemon_habilidades("Excavar");
        eevee.setId_evolucion(Long.valueOf(102));
        
        pokemonServiceImpl.eliminar(eevee);
        
        Mockito.verify(pokemonDaoMock, times(1)).delete(eevee);
    }
    
    @Test
    void test04PidoBorrarElID101YVerificoQueSeHayaLlamadoAlMetodoParaBorrarLaID(){
        pokemonServiceImpl.eliminarPorId(Long.valueOf(101));
        
        Mockito.verify(pokemonDaoMock, times(1)).deleteById(Long.valueOf(101));
    }
    
    @Test
    void test05PidoQueMeDevuelvaElPokemonSnorlaxYVerificoQueSeLlameAlMetodoParaEncontrarlo(){
        Pokemon snorlax = new Pokemon();
        snorlax.setId_pokemon(Long.valueOf(100));
        snorlax.setPokemon_nombre("Snorlax");
        snorlax.setPokemon_nivel(Long.valueOf(30));
        snorlax.setPokemon_tipos("Normal");
        snorlax.setPokemon_habilidades("Ronquido");
        snorlax.setId_evolucion(Long.valueOf(0));
        
        Pokemon pokemonEsperado = pokemonServiceImpl.encontrarPokemon(snorlax);
        
        Assertions.assertEquals(Long.valueOf(100), pokemonEsperado.getId_pokemon());
        Assertions.assertEquals("Snorlax", pokemonEsperado.getPokemon_nombre());
        Assertions.assertEquals(Long.valueOf(30), pokemonEsperado.getPokemon_nivel());
        Assertions.assertEquals("Normal", pokemonEsperado.getPokemon_tipos());
        Assertions.assertEquals("Ronquido", pokemonEsperado.getPokemon_habilidades());
        Assertions.assertEquals(Long.valueOf(0), pokemonEsperado.getId_evolucion());
        
        Mockito.verify(pokemonDaoMock, times(1)).findById(Long.valueOf(100));
    }
    
    @Test
    void test06PidoQueMeDevuelvaElPokemonConID100YMeDevuelveASnorlax(){
        Pokemon pokemonEsperado = pokemonServiceImpl.encontrarPokemonPorId(Long.valueOf(100));
        
        Assertions.assertEquals(Long.valueOf(100), pokemonEsperado.getId_pokemon());
        Assertions.assertEquals("Snorlax", pokemonEsperado.getPokemon_nombre());
        Assertions.assertEquals(Long.valueOf(30), pokemonEsperado.getPokemon_nivel());
        Assertions.assertEquals("Normal", pokemonEsperado.getPokemon_tipos());
        Assertions.assertEquals("Ronquido", pokemonEsperado.getPokemon_habilidades());
        Assertions.assertEquals(Long.valueOf(0), pokemonEsperado.getId_evolucion());
        
        Mockito.verify(pokemonDaoMock, times(1)).findById(Long.valueOf(100));
    }
    
    @Test
    void test07PidoQueMeDevuelvanLaEvolucionDelPokemonEeveeYVerificoQueMeDevuelvaLaCorrecta(){
        Pokemon eevee = new Pokemon();
        eevee.setId_pokemon(null);
        eevee.setPokemon_nombre("Eevee");
        eevee.setPokemon_nivel(Long.valueOf(30));
        eevee.setPokemon_tipos("Normal");
        eevee.setPokemon_habilidades("Excavar");
        eevee.setId_evolucion(Long.valueOf(102));
        
        Pokemon pokemonEvolucion = pokemonServiceImpl.encontrarEvolucion(eevee);
        
        Assertions.assertEquals(Long.valueOf(102), pokemonEvolucion.getId_pokemon());
        Assertions.assertEquals("Flareon", pokemonEvolucion.getPokemon_nombre());
        Assertions.assertEquals(Long.valueOf(40), pokemonEvolucion.getPokemon_nivel());
        Assertions.assertEquals("Fuego", pokemonEvolucion.getPokemon_tipos());
        Assertions.assertEquals("Mordisco", pokemonEvolucion.getPokemon_habilidades());
        Assertions.assertEquals(Long.valueOf(0), pokemonEvolucion.getId_evolucion());
        
        Mockito.verify(pokemonDaoMock, times(1)).findById(Long.valueOf(102));
    }
}
